# BambangShop Publisher App
Tutorial and Example for Advanced Programming 2024 - Faculty of Computer Science, Universitas Indonesia

---

## About this Project
In this repository, we have provided you a REST (REpresentational State Transfer) API project using Rocket web framework.

This project consists of four modules:
1.  `controller`: this module contains handler functions used to receive request and send responses.
    In Model-View-Controller (MVC) pattern, this is the Controller part.
2.  `model`: this module contains structs that serve as data containers.
    In MVC pattern, this is the Model part.
3.  `service`: this module contains structs with business logic methods.
    In MVC pattern, this is also the Model part.
4.  `repository`: this module contains structs that serve as databases and methods to access the databases.
    You can use methods of the struct to get list of objects, or operating an object (create, read, update, delete).

This repository provides a basic functionality that makes BambangShop work: ability to create, read, and delete `Product`s.
This repository already contains a functioning `Product` model, repository, service, and controllers that you can try right away.

As this is an Observer Design Pattern tutorial repository, you need to implement another feature: `Notification`.
This feature will notify creation, promotion, and deletion of a product, to external subscribers that are interested of a certain product type.
The subscribers are another Rocket instances, so the notification will be sent using HTTP POST request to each subscriber's `receive notification` address.

## API Documentations

You can download the Postman Collection JSON here: https://ristek.link/AdvProgWeek7Postman

After you download the Postman Collection, you can try the endpoints inside "BambangShop Publisher" folder.
This Postman collection also contains endpoints that you need to implement later on (the `Notification` feature).

Postman is an installable client that you can use to test web endpoints using HTTP request.
You can also make automated functional testing scripts for REST API projects using this client.
You can install Postman via this website: https://www.postman.com/downloads/

## How to Run in Development Environment
1.  Set up environment variables first by creating `.env` file.
    Here is the example of `.env` file:
    ```bash
    APP_INSTANCE_ROOT_URL="http://localhost:8000"
    ```
    Here are the details of each environment variable:
    | variable              | type   | description                                                |
    |-----------------------|--------|------------------------------------------------------------|
    | APP_INSTANCE_ROOT_URL | string | URL address where this publisher instance can be accessed. |
2.  Use `cargo run` to run this app.
    (You might want to use `cargo check` if you only need to verify your work without running the app.)

## Mandatory Checklists (Publisher)
-   [X] Clone https://gitlab.com/ichlaffterlalu/bambangshop to a new repository.
-   **STAGE 1: Implement models and repositories**
    -   [X] Commit: `Create Subscriber model struct.`
    -   [X] Commit: `Create Notification model struct.`
    -   [X] Commit: `Create Subscriber database and Subscriber repository struct skeleton.`
    -   [X] Commit: `Implement add function in Subscriber repository.`
    -   [X] Commit: `Implement list_all function in Subscriber repository.`
    -   [X] Commit: `Implement delete function in Subscriber repository.`
    -   [X] Write answers of your learning module's "Reflection Publisher-1" questions in this README.
-   **STAGE 2: Implement services and controllers**
    -   [X] Commit: `Create Notification service struct skeleton.`
    -   [X] Commit: `Implement subscribe function in Notification service.`
    -   [X] Commit: `Implement subscribe function in Notification controller.`
    -   [X] Commit: `Implement unsubscribe function in Notification service.`
    -   [X] Commit: `Implement unsubscribe function in Notification controller.`
    -   [X] Write answers of your learning module's "Reflection Publisher-2" questions in this README.
-   **STAGE 3: Implement notification mechanism**
    -   [X] Commit: `Implement update method in Subscriber model to send notification HTTP requests.`
    -   [X] Commit: `Implement notify function in Notification service to notify each Subscriber.`
    -   [X] Commit: `Implement publish function in Program service and Program controller.`
    -   [X] Commit: `Edit Product service methods to call notify after create/delete.`
    -   [X] Write answers of your learning module's "Reflection Publisher-3" questions in this README.

## Your Reflections
This is the place for you to write reflections:

### Mandatory (Publisher) Reflections

#### Reflection Publisher-1
##### Necessity of an Interface (Trait) in Rust
In the Observer pattern, the purpose of an interface (or a trait in Rust) is to define a common protocol for objects that need to be notified of changes in another object (the subject). In the BambangShop scenario, if all subscribers are expected to handle updates in a uniform way, using a single implementation (a single `Model` struct), might seem sufficient initially.

However, having a `Subscriber` trait has several advantages:
1. **Flexibility**: The trait allows the system to easily integrate different types of subscribers without modifying the existing codebase, keeping the system open for extension but closed for modification (Open-Closed Principle).
2. **Decoupling**: It separates what notifications are from how they are processed by the subscriber. The `BambangShop` doesn't need to know the specific details of how a subscriber is handling the data.
3. **Maintainability and Scalability**: As the application evolves, new types of subscribers handling specific kinds of updates can be added seamlessly without affecting the existing subscribers or the subject.

Considering these points, maintaining a `Subscriber` trait in the BambangShop case would generally be considered best practice.

##### Use of Vec vs. DashMap for Unique Identifiers
- **Vec**: Using a `Vec` to store subscribers would be straightforward but inefficient for managing unique attributes like IDs. For instance, every time you need to search, add, or remove a subscriber, you would potentially need to iterate over the array, leading to O(n) complexity operations.
- **DashMap**: Using `DashMap`, which is a thread-safe, concurrent map, allows for more efficient operations keyed by unique identifiers (like `id` for programs or `url` for subscribers). The use of a map reduces search, insert, and delete operations to average O(1) time complexity. This is especially useful in scenarios where high performance with concurrent access is required.

Given the requirements for unique identifiers and potentially high frequency of updates and queries, using `DashMap` makes more sense than `Vec` in this instance.

##### DashMap vs. Singleton Pattern
- **DashMap**: This has been used here as a thread-safe, concurrent map to manage a collection of subscribers that can be accessed and modified safely across multiple threads.
- **Singleton Pattern**: This pattern involves creating a single instance of a class that is accessible globally. While a singleton could manage access to a global resource like the list of subscribers, it doesn't inherently solve concurrency issues unless specifically designed to do so (e.g., using locking mechanisms).

Using `DashMap` directly is advantageous as it is tailor-made to be thread-safe and performant for concurrent read/write access, mitigating the need for additional synchronization logic that a singleton implementation would necessitate.

#### Reflection Publisher-2
##### Why Separate Service and Repository from Model
While the Model in MVC traditionally encompasses both data access (like repositories) and business logic (akin to services), there are compelling design principles that advocate for their separation:

1. **Single Responsibility Principle (SRP)**: By separating concerns, the Model can focus solely on representing the data and its immediate behaviors, while the Repository handles data storage/retrieval, and Service manages higher-level business logic. SRP ensures each class/module performs only one function and hence is easier to maintain and scale.

2. **Separation of Concerns (SoC)**: This principle advises splitting a software application into distinct sections, where each section addresses a separate concern. A Repository abstracts the data layer, providing a clear interface for data access that is independent of the data source's nature. A Service layer abstracts the business logic, decoupling it from both the data access layer and the user interface.

3. **Open/Closed Principle**: By isolating business logic from data access, enhancements in business rules or data access strategies can be implemented with minimal changes to the other parts. A well-defined interface to each allows extending their behaviors without modifying existing code directly.

##### Potential Issues with Model Encompassing All Responsibilities
If we rely solely on the Model for handling data manipulation, business logic, and perhaps even data persistence (retrieving and storing), several complexities and inefficiencies could arise:

- **Increased Code Complexity**: Models become bloated and harder to understand or modify. Logic for different concerns can get intermixed, making it difficult to trace and debug.
- **Harder to Maintain**: Changes in the business logic may require changes in the data access code and vice versa, increasing the risk of introducing bugs.
- **Less Reusable**: Tightly coupled data access and business logic mean that you can't reuse or repurpose them independently.
- **Scalability Issues**: With mixed concerns, scaling the application in terms of performance and feature enhancements becomes challenging, as modifications in one area might necessitate changes in others.

##### Interaction Complexity Among Unified Models
Imagine having models like `Program`, `Subscriber`, and `Notification` where each model directly interacts with the database and embeds business rules:

- **Complex Dependencies**: Each model might depend on the others for data leading to tightly coupled classes. Changes in one model could necessitate changes in others.
- **Reusability Compromised**: Duplicating logic across models for similar operations, like notifications, can lead to inconsistent behavior and bugs.
- **Difficulties in Testing**: Testing models that involve integrated business and data-access logic can be cumbersome without the ability to isolate behaviors neatly.

##### Postman: A Tool for API Testing and More

Postman is an API platform for building and testing APIs. It is extremely useful in backend and full-stack development, including group projects, due to its capabilities:

- **API Requests**: Send different types of HTTP requests (GET, POST, DELETE, etc.) to test API endpoints.
- **Environment and Variable Management**: Define environments for development, testing, production, etc., and manage variables that can be used across requests.
- **Pre-scripts and Tests**: Write scripts that execute before or after a request to validate outputs.
- **Automation**: Automate tests using the Collection Runner or integrate with CI/CD pipelines using Newman.
- **Documentation**: Automatically generate and share documentation for your API.
- **Collaboration**: Share collections and environments with your team, facilitating collaboration.

**Features of Interest**:
- **Automated Testing**: Useful for regression tests every time APIs are updated.
- **Mock Servers**: Create mock servers to simulate API endpoints without having to set up a backend.
- **Monitoring**: Set up monitors on collections to check the health and performance of APIs continuously.

#### Reflection Publisher-3
##### Variation Used in the Tutorial Case
The specific variation (Push or Pull) used in an Observer pattern scenario depends on how the data exchange between the publisher (subject) and the subscribers is handled. 

1. **Push Model**: The publisher sends updates to the subscribers along with all the necessary data needed by the subscribers. The subscribers receive updates passively.
2. **Pull Model**: The publisher notifies the subscribers of the change, but the subscribers must actively request the data pertinent to the update.

Based on a typical Observer pattern implementation in many tutorial and practical cases, if not explicitly mentioned, it's often a **Push model**. This is especially common in scenarios where the state changes need to be actively and immediately reflected to all subscribers without requiring them to check back with the publisher.

##### Advantages and Disadvantages of Using the Pull Model

If the tutorial were to use a Pull model instead of a Push model, the dynamics would change:

###### Advantages of Pull Model:
1. **Reduces Overhead in Notifications**: The publisher does not need to send detailed data with each notification, which can reduce the data transmitted over the network, particularly when only a subset of the update is relevant to the subscriber.
2. **Enable Subscribers to Manage State**: Subscribers can pull data based on their readiness or need, which could help in managing resources, especially in environments where updates are frequent but not all updates are relevant for every subscriber.
3. **Flexibility for Subscribers**: Each subscriber can decide what specific information to request, potentially minimizing irrelevant data processing.

###### Disadvantages of Pull Model:
1. **Increased Latency**: Since each subscriber has to make additional requests to get the actual data, this could introduce a latency between the time of actual change and the time subscribers respond to that change.
2. **Higher Load on Publisher**: If many subscribers need updates frequently, the publisher might face a high volume of incoming requests for data, leading to increased load and performance issues.
3. **Complexity in Subscribers**: Subscribers need to handle part of the logic to determine when and what to pull, making them more complex.

##### Impact of Non-multi-threaded Notification Process

Omitting multi-threading in the notification process has certain implications:

1. **Serial Processing**: Notifications to subscribers would be processed in a serial manner. If one subscriber’s process is slow or blocks (e.g., due to resource-intensive tasks or poor network conditions), it will delay the notification to subsequent subscribers.
2. **UI Freezing**: If the Observer pattern manages updates that affect user interfaces, UI updates would be synchronous and could freeze the UI during the processing of notifications.
3. **Scalability Issues**: As the number of subscribers grows, the time taken to notify all subscribers could become impractically long, making the system less scalable.
4. **Simplicity in Implementation**: While there are clear disadvantages in performance and responsiveness, avoiding multi-threading simplifies the implementation, as it eliminates the need for handling concurrency issues such as race conditions or deadlocks.
